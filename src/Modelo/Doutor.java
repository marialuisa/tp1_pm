package Modelo;

public class Doutor extends Mestre {

    private int qtdMestrandosOrientados;
    private int qtdDoutorandosOrientados;

    public Doutor(int qtdMestrandosOrientados, int qtdDoutorandosOrientados, int qtdGraduandosOrientados, int qtdHorasIC, int qtdHorasEstDoc, int id) {
        super(qtdGraduandosOrientados, qtdHorasIC, qtdHorasEstDoc, id);
        this.qtdMestrandosOrientados = qtdMestrandosOrientados;
        this.qtdDoutorandosOrientados = qtdDoutorandosOrientados;
    }

    @Override
    public void calcularPopularidade() {
        popularidade = peso + qtdCitacoes + ordemAutoria.size()
                + qtdHorasIC + qtdHorasEstDoc + (10 * qtdGraduandosOrientados)
                + (20 * qtdMestrandosOrientados) + (30 * qtdDoutorandosOrientados);
    }
}
