package Modelo;

public abstract class Veiculo {

    protected int id;
    protected double fatorImpacto;
    protected int qtdCitacoes;
    protected int numArtigos;

    public Veiculo(int id) {
        this.id = id;
        qtdCitacoes = 0;
        fatorImpacto = 0.0;
        numArtigos = 0;
    }
    public int getId(){
    	return this.id;
    }
    
    public void calcularFatorImpacto() {
        fatorImpacto = qtdCitacoes / numArtigos;
    }

    public void incrementarNumArtigos() {
        numArtigos++;
    }

    protected double getFatorImpacto() {
        return fatorImpacto;
    }

    public void incrementarCitacoes() {
        qtdCitacoes++;
    }

}
