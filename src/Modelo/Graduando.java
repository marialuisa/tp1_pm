package Modelo;

public class Graduando extends Pesquisador {

    protected int qtdHorasIC;
    protected int qtdHorasEstDoc;

    public Graduando(int qtdHorasIC, int qtdHorasEstDoc, int id) {
        super(id);
        this.qtdHorasIC = qtdHorasIC;
        this.qtdHorasEstDoc = qtdHorasEstDoc;
    }

    @Override
    public void calcularPopularidade() {
        popularidade = peso + qtdCitacoes + ordemAutoria.size() + qtdHorasIC + qtdHorasEstDoc;
        System.out.print(id + " " + peso + " " + qtdCitacoes + " " + ordemAutoria.size() + " " + qtdHorasIC + " " + qtdHorasEstDoc + "\n");
    }

}
