package Modelo;

public class Revista extends Veiculo {

    public Revista(int id) {
        super(id);
    }

    @Override
    public void calcularFatorImpacto() {
        fatorImpacto = (qtdCitacoes / numArtigos) + 2;
    }
}
