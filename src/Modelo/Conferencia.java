package Modelo;

public class Conferencia extends Veiculo {

    public Conferencia(int id) {
        super(id);
    }

    @Override
    public void calcularFatorImpacto() {
        fatorImpacto = (qtdCitacoes / numArtigos) + 1;
    }
}
