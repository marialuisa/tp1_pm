package Modelo;

import java.util.ArrayList;
import java.util.Iterator;

public class Artigo {

    private final int id;
    private final Veiculo v;
    private ArrayList<Pesquisador> autores;
    private double qualidade;
    private int qtdCitacoes;

    public Artigo(int id, Veiculo v) {
        this.id = id;
        this.v = v;
        this.v.incrementarNumArtigos();
        qtdCitacoes = 0;
        qualidade = 0.0;
        autores = new ArrayList<>();
    }
    public int getId(){
    	return this.id;
    }
    
    public boolean addAutor(Pesquisador p, int ordemAutoria) {
        return p.addPublicacao(ordemAutoria) && autores.add(p);
    }

    public void incrementarCitacoes() {
        Iterator it;

        qtdCitacoes++;
        it = autores.iterator();

        while (it.hasNext()) {
            ((Pesquisador) it.next()).incrementarCitacoes(1);
        }

        v.incrementarCitacoes();
    }

    public void calcularQualidade() {
        qualidade = v.getFatorImpacto() * qtdCitacoes;
    }

    public double getQualidade() {
        return qualidade;
    }
    
    public int getCitacoes(){
    	return this.qtdCitacoes;
    }

}
