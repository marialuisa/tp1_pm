package Modelo;

public class Mestre extends Graduando {

    protected int qtdGraduandosOrientados;

    public Mestre(int qtdGraduandosOrientados, int qtdHorasIC, int qtdHorasEstDoc, int id) {
        super(qtdHorasIC, qtdHorasEstDoc, id);
        this.qtdGraduandosOrientados = qtdGraduandosOrientados;
    }

    @Override
    public void calcularPopularidade() {
        popularidade = peso + qtdCitacoes + ordemAutoria.size()
                + qtdHorasIC + qtdHorasEstDoc + (10 * qtdGraduandosOrientados);
        
        System.out.print(id + " " + peso + " " + qtdCitacoes + " " + ordemAutoria.size() + " " + qtdHorasIC + " " + qtdHorasEstDoc + "\n");
    }
}
