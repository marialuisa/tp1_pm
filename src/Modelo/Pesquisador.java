package Modelo;

import java.util.ArrayList;
import java.util.Iterator;

public abstract class Pesquisador {

    protected int id;
    protected double peso;
    protected double popularidade;
    protected int qtdCitacoes;
    protected ArrayList<Integer> ordemAutoria;

    public Pesquisador(int id) {
        this.id = id;
        qtdCitacoes = 0;
        ordemAutoria = new ArrayList<>();
    }
    public int getId(){
    	return this.id;
    }
    
    public boolean addPublicacao(int lugarAutoria) {
        return ordemAutoria.add(lugarAutoria);
    }

    public void incrementarCitacoes(int i) {
        qtdCitacoes+= i;
    }

    public void calcularPeso() {
        Iterator it = ordemAutoria.iterator();

        while (it.hasNext()) {
            peso += (1 / ((Integer) it.next()));
            
            
        }
    }

    public void calcularPopularidade() {
        popularidade = peso + qtdCitacoes + ordemAutoria.size();
    }

    public double getPopularidade() {
        return popularidade;
    }
    
    public void getOrdemAutoria(){
    	Iterator it = ordemAutoria.iterator();
    	System.out.print("Autorias: ");
        while (it.hasNext()) {
            System.out.print((Integer) it.next() + " ");
        }
        System.out.print("\n");
    }
    
    public int getQtdCitacoes(){
    	return this.qtdCitacoes;
    }
    
}
