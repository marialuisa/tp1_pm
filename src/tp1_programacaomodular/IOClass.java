/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1_programacaomodular;

import Modelo.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.BufferedReader; 
import java.io.BufferedWriter; 
import java.io.FileReader; 
import java.io.FileWriter; 
import java.io.IOException;

/**
 *
 * @author Thiago
 */
public class IOClass {
    private String pathPesquisadores;
    private String pathGrafoPesquisadores;
    private String pathArtigosPesquisadores;
    private String pathCitacoes;
    private String pathVeiculos;
    private String pathArtigosVeiculos;
    
    private String pathSaidaPesquisadores;
    private String pathSaidaVeiculos;
    private String pathSaidaArtigos;

    public IOClass(String pathPesquisadores, String pathGrafoPesquisadores, String pathArtigosPesquisadores, String pathCitacoes, String pathVeiculos, String pathArtigosVeiculos, String pathSaidaPesquisadores, String pathSaidaVeiculos, String pathSaidaArtigos) {
        this.pathPesquisadores = pathPesquisadores;
        this.pathGrafoPesquisadores = pathGrafoPesquisadores;
        this.pathArtigosPesquisadores = pathArtigosPesquisadores;
        this.pathCitacoes = pathCitacoes;
        this.pathVeiculos = pathVeiculos;
        this.pathArtigosVeiculos = pathArtigosVeiculos;
        this.pathSaidaPesquisadores = pathSaidaPesquisadores;
        this.pathSaidaVeiculos = pathSaidaVeiculos;
        this.pathSaidaArtigos = pathSaidaArtigos;
    }
    
    public ArrayList<Veiculo> lerVeiculos(){
        String linha = "";
        
        int id_veiculo;
        char tag_veiculo;
        
        String[] dados;
        FileReader arq;
        BufferedReader lerArq;
        ArrayList<Veiculo> veiculos = new ArrayList<>();
        Veiculo v;
        int i = 0;
                  
        try { 
        	arq = new FileReader(pathVeiculos); 
        	lerArq = new BufferedReader(arq); 
        	linha = lerArq.readLine(); 

        	while (linha != null) {
        		        		
        		dados = linha.split(";");
        		id_veiculo =  Integer.valueOf(dados[0]);
        		tag_veiculo = dados[1].charAt(0);
        	    
        		switch(tag_veiculo){
                case 'R':
                    v = new Revista(id_veiculo);
                    break;
                
                case 'C':
                    v = new Conferencia(id_veiculo);
                    break;
                default:
                	v = new Revista(id_veiculo);
                  
        		}
        		veiculos.add(v);
        		
        		linha = lerArq.readLine();
        		
        	} arq.close(); 
        } catch (IOException e) {
        	System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage()); 
        }
        System.out.println();
        
        return veiculos;
    }
    
    public ArrayList<Pesquisador> lerPesquisadores(){
        String linha = "";
        
        int id_pesquisador;
        char tipo_pesquisador;
        int horas_ic;
        int horas_estagio;
        int num_grad_orient;
        int num_M_orient;
        int num_D_orient;
        
        String[] dados;
        FileReader arq;
        BufferedReader lerArq;
        ArrayList<Pesquisador> pesquisadores = new ArrayList<>();
        Pesquisador p;
        int i = 0;
                  
        try { 
        	arq = new FileReader(pathPesquisadores); 
        	lerArq = new BufferedReader(arq); 
        	linha = lerArq.readLine(); 

        	while (linha != null) {
        		        		
        		dados = linha.split(";");
        		id_pesquisador =  Integer.valueOf(dados[0]);
        		tipo_pesquisador = dados[1].charAt(0);
        		horas_ic = Integer.valueOf(dados[2]);
        	    horas_estagio = Integer.valueOf(dados[3]);
        	    num_grad_orient = Integer.valueOf(dados[4]);
        	    num_M_orient = Integer.valueOf(dados[5]);
        	    num_D_orient = Integer.valueOf(dados[6]);
        	    
        		switch(tipo_pesquisador){
                case 'G':
                    p = new Graduando(horas_ic, horas_estagio, id_pesquisador);
                    break;
                
                case 'M':
                    p = new Mestre(num_grad_orient, horas_ic, horas_estagio, id_pesquisador);
                    break;
                case 'D':
                    p = new Doutor(num_M_orient, num_D_orient, num_grad_orient, horas_ic, horas_estagio, id_pesquisador);
                    break;
                default:
                    p = new Graduando(horas_ic, horas_estagio, id_pesquisador);
        		}
        		
        		pesquisadores.add(p);
        		
        		linha = lerArq.readLine();
        		
        	} arq.close(); 
        } catch (IOException e) {
        	System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage()); 
        }

                
        return pesquisadores;
    }
    
    public ArrayList<Artigo> lerRelacaoArtigoVeiculo(ArrayList<Veiculo> veiculos){
        String linha = "";
        
        int id_artigo;
        int id_veiculo;
        
        String[] dados;
        FileReader arq;
        BufferedReader lerArq;
        ArrayList<Artigo> artigos = new ArrayList<>();
       	Artigo a;
       	Veiculo v = new Revista(0);
        
        try { 
        	arq = new FileReader(pathArtigosVeiculos); 
        	lerArq = new BufferedReader(arq); 
        	linha = lerArq.readLine(); 

        	while (linha != null) {
        		        		
        		dados = linha.split(";");
        		id_artigo =  Integer.valueOf(dados[0]);
        		id_veiculo = Integer.valueOf(dados[1]);
        	    
        		a = new Artigo(id_artigo, v);
        		
        		for (Iterator<Veiculo> it = veiculos.iterator(); it.hasNext();) {
        			 v = it.next();
	        		 if(v.getId() == id_veiculo){
	        			 a = new Artigo(id_artigo, v);
	        		 }
           		} 

        		artigos.add(a);
        		
        		linha = lerArq.readLine();
        		
        	} 
        	
        	arq.close(); 
        } catch (IOException e) {
        	System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage()); 
        }
        System.out.println();
        
        return artigos;
    }
    
    public void percorreVetorPesquisadores(ArrayList<Pesquisador> Pesquisadores){
    	Pesquisador pq;
    	for (Iterator<Pesquisador> p = Pesquisadores.iterator(); p.hasNext();) {    
    		 pq = p.next();
    		 System.out.println(pq.getId() + " \ncitacoes: " + pq.getQtdCitacoes()); 
    		 pq.getOrdemAutoria();
    		} 
    }
    public void percorreVetorVeiculos(ArrayList<Veiculo> veiculos){
    	for (Iterator<Veiculo> it = veiculos.iterator(); it.hasNext();) {    
    		 System.out.println(it.next().getId()); 
    		} 
    }
    public void percorreVetorArtigos(ArrayList<Artigo> Artigos){
    	Artigo a;
    	for (Iterator<Artigo> it = Artigos.iterator(); it.hasNext();) {
    		 a = it.next();
    		 System.out.print(a.getId() + " "); 
    		 System.out.print(a.getCitacoes() + "\n"); 
    	} 
    	
    }
    
    public void lerRelacaoArtigoPesquisador(ArrayList<Pesquisador> Pesquisadores, ArrayList<Artigo> artigos){
        String linha = "";
        int id_pesquisador;
        int id_artigo;
        int ordem_autoria;
        Pesquisador pq;
        String[] dados;
        FileReader arq;
        BufferedReader lerArq;
        Artigo a;
        int citacoesArtigo = 0;
        
        try { 
        	arq = new FileReader(pathArtigosPesquisadores); 
        	lerArq = new BufferedReader(arq); 
        	linha = lerArq.readLine(); 

        	while (linha != null) {
        		        		
        		dados = linha.split(";");
        		id_artigo = Integer.valueOf(dados[0]);
        		id_pesquisador =  Integer.valueOf(dados[1]);
        	    ordem_autoria = Integer.valueOf(dados[2]);
        	    
        		linha = lerArq.readLine();
        		
        		for (Iterator<Artigo> ats = artigos.iterator(); ats.hasNext();) {    
   	           	 a = ats.next();
   	       		 if(a.getId() == id_artigo){
   	       			 citacoesArtigo = a.getCitacoes();
   	       		 }
              	}
        		
        		for (Iterator<Pesquisador> it = Pesquisadores.iterator(); it.hasNext();) {    
	           	 pq = it.next();
	       		 if(pq.getId() == id_pesquisador){
	       			 pq.addPublicacao(ordem_autoria);
	       			 pq.incrementarCitacoes(citacoesArtigo);
	       		 }
           		} 
        	} 
        	arq.close();

        } catch (IOException e) {
        	System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage()); 
        }
    }

    public void lerCitacoes(ArrayList<Artigo> artigos){
        String linha = "";
        int id_artigoA;
        int id_artigoB;

        Artigo a;
        String[] dados;
        FileReader arq;
        BufferedReader lerArq;
                 
        try { 
        	arq = new FileReader(pathCitacoes); 
        	lerArq = new BufferedReader(arq); 
        	linha = lerArq.readLine(); 

        	while (linha != null) {
        		        		
        		dados = linha.split(";");
        		id_artigoA = Integer.valueOf(dados[0]);
        		id_artigoB =  Integer.valueOf(dados[1]);
        	    
        		linha = lerArq.readLine();
        		
        		for (Iterator<Artigo> it = artigos.iterator(); it.hasNext();) {    
	           	 a = it.next();
	       		 if(a.getId() == id_artigoA){
	       			 a.incrementarCitacoes();
	       		 }
           		} 
        	} 
        	arq.close();

        } catch (IOException e) {
        	System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage()); 
        }
    }
    
    // essa funcao deve sair do IO futuramente
    public void calcularPesosPesquisadores(ArrayList<Pesquisador> arrayPesquisadores){
    	
    	for (Iterator<Pesquisador> it = arrayPesquisadores.iterator(); it.hasNext();) {    
    		Pesquisador pq = it.next();
    		pq.calcularPeso();
    		pq.calcularPopularidade();
    		
    		System.out.println(pq.getId() + " " + pq.getPopularidade());
       } 
    }
}
