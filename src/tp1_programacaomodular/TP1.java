/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp1_programacaomodular;

import java.util.ArrayList;
import java.util.Iterator;

import Modelo.*;

/**
 *
 * @authores Thiago Marques e Maria Luisa
 * 			 
 */

public class TP1 {                
    
    public static void main(String[] args) {
    	
    	String pathPesquisadores = "files\\pesquisadores.txt";
        String pathArtigosPesquisadores = "files\\grafo_artigos_pesquisadores.txt";
        String pathGrafoPesquisadores = "files\\grafo_pesquisadores.txt";
        String pathCitacoes = "files\\grafo_citacoes.txt";
        String pathVeiculos = "files\\veiculos.txt";
        String pathArtigosVeiculos = "files\\artigos_veiculos.txt";
        String pathSaidaPesquisadores = "";
        String pathSaidaVeiculos = "";
        String pathSaidaArtigos = "";
        
    	ArrayList<Pesquisador> arrayPesquisadores;
    	ArrayList<Artigo> arrayArtigos;
    	ArrayList<Veiculo> arrayVeiculos;
    	
        System.out.print("Tp1 rodando");
        
        IOClass IO = new IOClass(pathPesquisadores, pathGrafoPesquisadores, pathArtigosPesquisadores, pathCitacoes, pathVeiculos, pathArtigosVeiculos, pathSaidaPesquisadores, pathSaidaVeiculos, pathSaidaArtigos);
        
        arrayPesquisadores = IO.lerPesquisadores();
        arrayVeiculos = IO.lerVeiculos();
        arrayArtigos = IO.lerRelacaoArtigoVeiculo(arrayVeiculos);  
        IO.lerCitacoes(arrayArtigos);
        IO.lerRelacaoArtigoPesquisador(arrayPesquisadores, arrayArtigos); 
        
        
//        System.out.print("Pesquisadores lidos com sucesso\n");
//        IO.percorreVetorPesquisadores(arrayPesquisadores);
//        System.out.print("Veiculos lidos com sucesso\n");
        //IO.percorreVetorVeiculos(arrayVeiculos);
//        System.out.print("Artigos lidos com sucesso\n");
//        IO.percorreVetorArtigos(arrayArtigos);
        
        IO.calcularPesosPesquisadores(arrayPesquisadores);
    }
    
}
